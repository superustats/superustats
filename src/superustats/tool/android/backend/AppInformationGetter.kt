/*
Copyright (c) 2018 Hocuri

This file is part of SuperUStats.

SuperUStats is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SuperUStats is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SuperUStats.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This file contains functions that get necessary information about apps.
 */

package superustats.tool.android.backend

import android.app.usage.UsageStats
import android.app.usage.UsageStatsManager
import android.content.Context
import android.widget.TextView
import android.widget.Toast
import superustats.tool.android.R
import superustats.tool.android.userInterface.MainActivity
import java.sql.Date
import java.text.SimpleDateFormat


/**
 * Queries usage stats for the last two years by calling usageStatsManager.queryAndAggregateUsageStats().
 * TAKING VERY LONG, CALL ONLY ONCE DUE TO PERFORMANCE REASONS!
 *
 * @return A map with the package names of running apps or null if it could not be determined (on older versions of Android)
 * @see android.app.usage.UsageStatsManager.queryAndAggregateUsageStats
 */
internal fun getAggregatedUsageStats(mainActivity: MainActivity, startDate: Long): Map<String, UsageStats> {
	val usageStatsManager = mainActivity.getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager

	//Get all data starting two years ago
	val now = System.currentTimeMillis()
	//val startDate = now - 1000L*60*60*24*365*2

	val stats = usageStatsManager.queryAndAggregateUsageStats(startDate, now)

	// The Usagestats API will not tell you what the real start date was (it is usually not possible to manage the wanted start date)
	// so we have to find out ourselves: (see also https://gitlab.com/superustats/superustats/issues/2)
	val returnedStartDate = stats.values.map { it.firstTimeStamp }.min() ?: now

	val simpleDateFormat = SimpleDateFormat.getDateTimeInstance()
	val startTimeString = simpleDateFormat.format(Date(returnedStartDate))

	val string = mainActivity.getString(R.string.showing_stats_since) + " " + startTimeString
	Toast.makeText(mainActivity, string, Toast.LENGTH_LONG).show()
	mainActivity.findViewById<TextView>(R.id.textView2).text = string

	return stats
}

