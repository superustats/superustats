/*
Copyright (c) 2015 axxapy
Copyright (c) 2018 Hocuri

This file is part of SuperUStats.

SuperUStats is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SuperUStats is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SuperUStats.  If not, see <http://www.gnu.org/licenses/>.
*/

package superustats.tool.android.userInterface

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.SearchManager
import android.app.TimePickerDialog
import android.app.usage.UsageStats
import android.content.ComponentCallbacks2
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Bundle
import android.text.format.DateFormat
import android.view.Menu
import android.view.View
import android.widget.DatePicker
import android.widget.ProgressBar
import android.widget.TimePicker
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import superustats.tool.android.R
import superustats.tool.android.backend.expectNonNull
import superustats.tool.android.backend.getAggregatedUsageStats
import java.util.*

/**
 * The activity that is shown at startup
 */
class MainActivity : AppCompatActivity() {
	private lateinit var appsListAdapter: AppsListAdapter

	private lateinit var progressBar: ProgressBar

	internal var usageStatsMap: Map<String, UsageStats>? = null



	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		setContentView(R.layout.activity_main)

		val listView = list

		appsListAdapter = AppsListAdapter(this)
		listView.layoutManager = LinearLayoutManager(this)
		listView.adapter = appsListAdapter

		progressBar = progress
		progressBar.visibility = View.VISIBLE

		requestUsageStatsPermission(this) {
			val packages = packageManager.getInstalledPackages(PackageManager.GET_META_DATA)
			appsListAdapter.setAndLoadItems(packages)
		}

		setSupportActionBar(toolbar)

		chooseDateAndTimeAndRefresh()
	}

	override fun onResume() {
		super.onResume()

		//Execute all tasks and retain only those that returned true.
		toBeDoneOnResume.retainAll { it() }
	}

	private fun chooseDateAndTimeAndRefresh() {

		Toast.makeText(this, getString(R.string.choose_time), Toast.LENGTH_LONG).show()

		val chosenTime = Calendar.getInstance()

		class TimePickerFragment : DialogFragment(), TimePickerDialog.OnTimeSetListener {
			override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
				// Create a new instance of TimePickerDialog and return it
				return TimePickerDialog(this@MainActivity, this, 1, 0, DateFormat.is24HourFormat(activity))
			}
			override fun onTimeSet(view: TimePicker, hourOfDay: Int, minute: Int) {
				chosenTime.set(Calendar.HOUR_OF_DAY, hourOfDay)
				chosenTime.set(Calendar.MINUTE, minute)

				val now = Calendar.getInstance()
				val epoch = Calendar.getInstance()
				epoch.timeInMillis = 0

				if (chosenTime > now || chosenTime < epoch) {
					Toast.makeText(this@MainActivity, getString(R.string.start_time_not_valid), Toast.LENGTH_LONG).show()
					// Try again:
					chooseDateAndTimeAndRefresh()
					return
				}

				usageStatsMap = getAggregatedUsageStats(this@MainActivity, chosenTime.timeInMillis)

				appsListAdapter.refresh()
			}
		}

		class DatePickerFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {
			override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
				// Use the current date as the default date in the picker
				val c = Calendar.getInstance()
				val year = c.get(Calendar.YEAR)
				val month = c.get(Calendar.MONTH)
				val day = c.get(Calendar.DAY_OF_MONTH)
				// Create a new instance of DatePickerDialog and return it
				return DatePickerDialog(this@MainActivity, this, year, month, day)
			}
			override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
				chosenTime.set(Calendar.YEAR, year)
				chosenTime.set(Calendar.MONTH, month)
				chosenTime.set(Calendar.DAY_OF_MONTH, dayOfMonth)

				TimePickerFragment().show(supportFragmentManager, "timePicker")
			}
		}


		DatePickerFragment().show(supportFragmentManager, "datePicker")
	}

	/**
	 * At startup, there will be a spinning progress bar at the top right hand corner.
	 * Invoking this method will hide this progress bar.
	 */
	fun hideProgressBar() {
		progressBar.visibility = View.GONE
	}

	/**
	 * The method that is responsible for showing the search icon in the top right hand corner.
	 * @param menu The Menu to which the search icon is added.
	 */
	@SuppressLint("RestrictedApi")
	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		menuInflater.inflate(R.menu.main, menu)

		val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
		val searchView = menu.findItem(R.id.action_search).actionView as SearchView
		searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
		searchView.setOnQueryTextFocusChangeListener { _, queryTextFocused ->
			if (!queryTextFocused && searchView.query.isEmpty()) {
				val supportActionBar = supportActionBar
				supportActionBar?.expectNonNull(TAG)?.collapseActionView()
			}
		}
		searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
			override fun onQueryTextSubmit(s: String) = false

			override fun onQueryTextChange(s: String): Boolean {
				appsListAdapter.setSearchPattern(s)
				return true
			}
		})

		menu.findItem(R.id.time_picker).setOnMenuItemClickListener {
			chooseDateAndTimeAndRefresh()
			true // this click was 'consumed'
		}

		return super.onCreateOptionsMenu(menu)
	}

	override fun onConfigurationChanged(newConfig: Configuration?) {
		super.onConfigurationChanged(newConfig)

		//This is necessary so that the list items change their look when the screen is rotated:
		val listView = list
		val position = (listView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
		listView.adapter = null
		listView.layoutManager = null
		listView.recycledViewPool.clear()
		listView.adapter = appsListAdapter
		listView.layoutManager = LinearLayoutManager(this)
		appsListAdapter.notifyDataSetChanged()
		(listView.layoutManager as LinearLayoutManager).scrollToPosition(position)
	}

	override fun onTrimMemory(level: Int) {
		//See https://developer.android.com/topic/performance/memory#release

		when (level) {
			ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN,

			ComponentCallbacks2.TRIM_MEMORY_RUNNING_MODERATE -> { }

			ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW,
			ComponentCallbacks2.TRIM_MEMORY_RUNNING_CRITICAL,

			ComponentCallbacks2.TRIM_MEMORY_BACKGROUND,
			ComponentCallbacks2.TRIM_MEMORY_MODERATE,
			ComponentCallbacks2.TRIM_MEMORY_COMPLETE -> {
				appsListAdapter.trimMemory()
			}

			else -> {}
		}
	}

	companion object {
		private val toBeDoneOnResume: MutableList<() -> Boolean> = mutableListOf()
		/**
		 * Execute this task on resume.
		 * @param task If it returns true, then it will be executed again at the next onResume.
		 */
		internal fun doOnResume(task: ()->Boolean) {
			toBeDoneOnResume.add(task)
		}

	}

}

private const val TAG = "MainActivity"