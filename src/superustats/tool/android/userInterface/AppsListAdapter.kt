/*
Copyright (c) 2015 axxapy
Copyright (c) 2018 Hocuri

This file is part of SuperUStats.

SuperUStats is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SuperUStats is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SuperUStats.  If not, see <http://www.gnu.org/licenses/>.
*/

package superustats.tool.android.userInterface

import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Handler
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import superustats.tool.android.R
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.ThreadFactory
import java.util.concurrent.TimeUnit


/**
 * This class is responsible for viewing the list of installed apps.
 */
class AppsListAdapter internal constructor(private val mainActivity: MainActivity) : RecyclerView.Adapter<AppsListAdapter.ViewHolder>() {
    private val tFactory = ThreadFactory { r ->
        val t = Thread(r)
        t.isDaemon = true
        t
    }

    /**
     * This list contains the apps that are shown to the user. This is a subset of listOriginal.
     */
    private var list = emptyList<PackageInfo>()

    /**
     * This list contains all user apps.
     */
    private var listOriginal = emptyList<PackageInfo>()

    private val executorServiceIcons = Executors.newFixedThreadPool(3, tFactory)
    private val handler = Handler()
    private val packageManager: PackageManager = mainActivity.packageManager

    private val cacheAppName = Collections.synchronizedMap(LinkedHashMap<String, String>(10, 1.5f, true))
    private val cacheAppIcon = Collections.synchronizedMap(LinkedHashMap<String, Drawable>(10, 1.5f, true))

    private var searchPattern: String? = null

    internal inner class GuiLoader(private val viewHolder: ViewHolder, private val package_info: PackageInfo) : Runnable {

        override fun run() {
            var first = true
            do {
                try {
                    val appName = cacheAppName[package_info.packageName]
                            ?: package_info.applicationInfo.loadLabel(packageManager) as String

                    val icon = package_info.applicationInfo.loadIcon(packageManager)
                    cacheAppName[package_info.packageName] = appName
                    cacheAppIcon[package_info.packageName] = icon
                    handler.post {
                        viewHolder.setAppName(appName, searchPattern)
                        viewHolder.imgIcon.setImageDrawable(icon)
                    }


                } catch (ex: OutOfMemoryError) {
                    cacheAppIcon.clear()
                    cacheAppName.clear()
                    if (first) {
                        first = false
                        continue
                    }
                }

                break
            } while (true)
        }
    }

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v), OnClickListener {
        private val txtAppName: TextView = v.findViewById(R.id.txtAppName)
        private val txtExplanation: TextView = v.findViewById(R.id.txtExplanation)
        var imgIcon: ImageView = v.findViewById(R.id.imgIcon)

        init {
            v.setOnClickListener(this)
        }

        /**
         * This method defines what is done when a list item (that is, an app) is clicked.
         * @param v The clicked view.
         */
        override fun onClick(v: View) {
            //TODO onClick
        }

        fun bindTo(info: PackageInfo) {
            setAppName(
                    cacheAppName[info.packageName] ?: info.packageName,
                    searchPattern)

            imgIcon.setImageDrawable(cacheAppIcon[info.packageName])
            if (cacheAppIcon[info.packageName] == null) {
                executorServiceIcons.submit(GuiLoader(this, info))
            }

            txtExplanation.text = ((mainActivity
                    .usageStatsMap
                    ?.get(info.packageName)
                    ?.totalTimeInForeground ?: 0
                    ) / (1000L * 60) //Show time in minutes
                    ).toString() + " min"
        }

        fun setAppName(name: String, highlight: String?) {
            setAndHighlight(txtAppName, name, highlight)
        }

        private fun setAndHighlight(view: TextView, value: String, pattern: String?) {
            view.text = value
            if (pattern == null || pattern.isEmpty()) return // nothing to highlight

            val valueLower = value.toLowerCase()
            var offset = 0
            var index = valueLower.indexOf(pattern, offset)
            while (index >= 0 && offset < valueLower.length) {
                val span = SpannableString(view.text)
                span.setSpan(ForegroundColorSpan(Color.BLUE), index, index + pattern.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                view.text = span
                offset += index + pattern.length
                index = valueLower.indexOf(pattern, offset)
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        return ViewHolder(
                LayoutInflater.from(viewGroup.context).inflate(R.layout.list_item, viewGroup, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, i: Int) {
        val item = list[i]
        holder.bindTo(item)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setSearchPattern(pattern: String) {
        searchPattern = pattern.toLowerCase()
        refreshList()
        notifyDataSetChanged()
    }

    private fun refreshList() {
        list =
                if (searchPattern.isNullOrEmpty()) {
                    listOriginal.filter { mainActivity.usageStatsMap?.get(it.packageName)?.totalTimeInForeground ?: 0 > 60L*1000 }
                } else {

                    // When the user is searching, the more relevant apps (that is, those
                    // that start with the search pattern) are shown at the top:
                    val (importantApps, otherApps) =
                            listOriginal
                                    .asSequence()
                                    .filter { cacheAppName[it.packageName]?.toLowerCase()?.contains(searchPattern!!) == true }
                                    .partition {
                                        cacheAppName[it.packageName]?.toLowerCase()?.startsWith(searchPattern!!) != false
                                    }
                    importantApps + otherApps
                }
    }


    internal fun refresh() {

        //We need to test whether the applications are still installed and remove those that are not.
        //Apparently, there is no better way for this than trying to access the applicationInfo.
        listOriginal = listOriginal
                .filter {
                    try {
                        it.applicationInfo
                        true
                    } catch (e: PackageManager.NameNotFoundException) {
                        false
                    }
                }
                .sortedByDescending {
                    mainActivity.usageStatsMap?.get(it.packageName)?.totalTimeInForeground ?: 0
                }

        refreshList()
        notifyDataSetChanged()
    }



    internal fun setAndLoadItems(packages: List<PackageInfo>) {
        listOriginal = packages.sortedByDescending { mainActivity.usageStatsMap?.get(it.packageName)?.totalTimeInForeground ?: 0 }


        @Suppress("UNCHECKED_CAST")
        loadAllNames(listOriginal) {
            mainActivity.runOnUiThread {
                notifyDataSetChanged()
                mainActivity.hideProgressBar()
            }
        }

        refreshList()
        notifyDataSetChanged()
    }

    internal fun trimMemory() {
        cacheAppIcon.clear()
    }

    private fun loadAllNames(items: List<PackageInfo>, onAllNamesLoaded: () -> Unit) {
        val executorServiceNames = Executors.newFixedThreadPool(3, tFactory)
        for (item in items) {
            executorServiceNames.submit {
                cacheAppName[item.packageName] = item.applicationInfo.loadLabel(packageManager).toString()
            }
        }
        executorServiceNames.shutdown()
        Thread {
            val finished = executorServiceNames.awaitTermination(2, TimeUnit.MINUTES)
            if (!finished)
                Log.e(TAG, "After 2 minutes, some app names were still not loaded")
            onAllNamesLoaded()
        }.start()
    }

    companion object {
        private const val TAG = "AppsListAdapter"
    }

}
