SuperUStats
===========

An android app that views Android "usage stats" (statistics about what apps you are using how often).

Using this app is a little weird but this is due to the IMHO very bad usage stats API. Anyway, if you have any ideas how to improve it, you're welcome.

This is a fork from SuperFreezZ, using code from ApkExtractor.

Features
--------

* Search through apps
* does not need Internet access
* View even usages before SuperUStats was installed
* Coose the time since when usage shall be counted

Build
-----

The build should succeed out of the box with Android Studio and Gradle. If not, it is probably my fault, please open an issue then. Others will probably also have this problem then.

Contributing to SuperUStats
------------

If you have a problem or a question or an idea or whatever, just open an issue!

If you would like to help, have a look at the issues or think about what could be improved and open an issue for it. Please tell me what you are going to do to avoid that I also implement the same thing at the same time :-)

Donate
------

I do not accept monetary donations currently (I tried to set up a liberapay account but it was too much of an effort so I gave it up for the time being). 

However, to show me your support, you can [donate to WWF or the Christian Blind Mission and post about it here](https://gitlab.com/SuperFreezZ/SuperFreezZ/issues/18). I would maybe also have put Fairphone to the list as I really like the idea but there seems not to be an option to donate to them, either. So just let me point out here that Fairphone is selling an ethical and modular smartphone.

Credits
-------

I took the code to show the apps list from [apkExtractor](https://f-droid.org/wiki/page/axp.tool.apkextractor).

Copying
-------

```
Copyright (c) 2015 axxapy
Copyright (c) 2018 Hocuri

SuperUStats is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SuperUStats is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SuperUStats.  If not, see <http://www.gnu.org/licenses/>.
```

------------------------------------------------------------------

SuperUStats contains files additionally distributed under the MIT license. For these files you may choose between GPLv3-or-later (see above) and MIT (see below). These files are:

```
build.gradle
src/superustats/tool/android/userInterface/AppsListAdapter.kt
src/superustats/tool/android/userInterface/MainActivity.kt
res/layout/list_item.xml
res/layout/activity_main.xml
res/values/strings.xml
res/values/colors.xml
res/values/styles.xml
res/values/attrs.xml
res/values-de/strings.xml
res/xml/searchable.xml
res/menu/main.xml
AndroidManifest.xml
```

```
The MIT License (MIT)

Copyright (c) 2015 axxapy
Copyright (c) 2018 Hocuri

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
